package bewerbung_latex_golang

import (
	"fmt"
	"reflect"
	"strconv"
	"testing"
	"time"
)

func Test_csvReader(t *testing.T) {
	type args struct {
		csvPath string
	}
	var ttargs args
	ttargs.csvPath = ".testfiles/anschrift.csv"
	var ttwant [][]string
	ttwant0 := []string{
		"ID",
		"KW",
		"Datum",
		"Firma",
		"FirmaAnrede",
		"FirmaName",
		"Str",
		"PLZ",
		"Ort",
		"Stelle",
		"Rueckmeldung",
	}
	ttwant1 := []string{
		"1",
		"15",
		"15.04.2021",
		"Franco-Midland Hardware Co.",
		"Herr",
		"Pinner",
		"Hauptstr. 42",
		"12345",
		"Neustadt",
		"Geschäftsführer",
		"Einladung zum Vorstellungsgespräch",
	}
	ttwant = append(ttwant, ttwant0)
	ttwant = append(ttwant, ttwant1)
	tests := []struct {
		name    string
		args    args
		want    [][]string
		wantErr bool
	}{
		{"testing csvReader", ttargs, ttwant, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := csvReader(tt.args.csvPath)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewConfig() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if (!reflect.DeepEqual(got, tt.want)) && tt.wantErr == false {
				t.Errorf("NewConfig() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_csvWriter(t *testing.T) {
	type args struct {
		csvPath         string
		anschriftsarray [][]string
	}
	var ttargs args
	ttargs.csvPath = ".testfiles/anschrift.csv"
	ttargs0 := []string{
		"ID",
		"KW",
		"Datum",
		"Firma",
		"FirmaAnrede",
		"FirmaName",
		"Str",
		"PLZ",
		"Ort",
		"Stelle",
		"Rueckmeldung",
	}
	ttargs1 := []string{
		"1",
		"15",
		"15.04.2021",
		"Franco-Midland Hardware Co.",
		"Herr",
		"Pinner",
		"Hauptstr. 42",
		"12345",
		"Neustadt",
		"Geschäftsführer",
		"Einladung zum Vorstellungsgespräch",
	}
	ttargs.anschriftsarray = append(ttargs.anschriftsarray, ttargs0)
	ttargs.anschriftsarray = append(ttargs.anschriftsarray, ttargs1)
	tests := []struct {
		name string
		args args
	}{
		{"Testing csvWriter", ttargs},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			csvWriter(tt.args.csvPath, tt.args.anschriftsarray)
		})
	}
}

func Test_main(t *testing.T) {
	tests := []struct {
		name string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			main()
		})
	}
}

func TestNewConfig(t *testing.T) {
	type args struct {
		configPath string
	}
	var ttcfg Config
	ttcfg.Bewerber.Name = "Mustermann"
	ttcfg.Bewerber.Vorname = "Max"
	ttcfg.Bewerber.Straße = "Musterstraße 1"
	ttcfg.Bewerber.Plz = "12345"
	ttcfg.Bewerber.Stadt = "Musterdorf"
	ttcfg.Bewerber.Beruf = "DevSecOps Engineer"
	ttcfg.Bewerber.Email = "max@mustermann.com"
	ttcfg.Bewerber.Telefon = "01234~567890"
	ttcfg.Bewerber.Mobil = "0123~4567890"
	ttcfg.Bewerber.Staatsangehörigkeit = "deutsch"
	ttcfg.Bewerber.Geburtsdatum = "01.01.2000"
	var ttabz struct {
		Anlage string `yaml:"anlage"`
	}
	var ttaz struct {
		Anlage string `yaml:"anlage"`
	}
	var ttcert struct {
		Anlage string `yaml:"anlage"`
	}
	ttabz.Anlage = "Ausbildungszeugnis"
	ttcfg.Anhang = append(ttcfg.Anhang, ttabz)
	ttaz.Anlage = "Arbeitszeugnis"
	ttcfg.Anhang = append(ttcfg.Anhang, ttaz)
	ttcert.Anlage = "Zertifikate"
	ttcfg.Anhang = append(ttcfg.Anhang, ttcert)
	var ttargs args
	ttargs.configPath = ".testfiles/config.yaml"
	var ttargsfileerror args
	ttargsfileerror.configPath = ".testfiles/noconfig.yaml"
	var ttargsdecodeerror args
	ttargsdecodeerror.configPath = ".testfiles/configerror.yaml"
	tests := []struct {
		name    string
		args    args
		want    *Config
		wantErr bool
	}{
		{"Read Config to struct", ttargs, &ttcfg, false},
		{"Read nonexisting Config", ttargsfileerror, &ttcfg, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewConfig(tt.args.configPath)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewConfig() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if (!reflect.DeepEqual(got, tt.want)) && tt.wantErr == false {
				t.Errorf("NewConfig() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_createConfigContent(t *testing.T) {
	type args struct {
		bcfg        *Config
		anschriften [][]string
	}
	var ttcfg Config
	ttcfg.Bewerber.Name = "Mustermann"
	ttcfg.Bewerber.Vorname = "Max"
	ttcfg.Bewerber.Straße = "Musterstraße 1"
	ttcfg.Bewerber.Plz = "12345"
	ttcfg.Bewerber.Stadt = "Musterdorf"
	ttcfg.Bewerber.Beruf = "DevSecOps Engineer"
	ttcfg.Bewerber.Email = "max@mustermann.com"
	ttcfg.Bewerber.Telefon = "01234~567890"
	ttcfg.Bewerber.Mobil = "0123~4567890"
	ttcfg.Bewerber.Staatsangehörigkeit = "deutsch"
	ttcfg.Bewerber.Geburtsdatum = "01.01.2000"
	var ttabz struct {
		Anlage string `yaml:"anlage"`
	}
	var ttaz struct {
		Anlage string `yaml:"anlage"`
	}
	var ttcert struct {
		Anlage string `yaml:"anlage"`
	}
	ttabz.Anlage = "Ausbildungszeugnis"
	ttcfg.Anhang = append(ttcfg.Anhang, ttabz)
	ttaz.Anlage = "Arbeitszeugnis"
	ttcfg.Anhang = append(ttcfg.Anhang, ttaz)
	ttcert.Anlage = "Zertifikate"
	ttcfg.Anhang = append(ttcfg.Anhang, ttcert)
	var ttargs args
	ttargs.bcfg = &ttcfg
	ttargs0 := []string{
		"ID",
		"KW",
		"Datum",
		"Firma",
		"FirmaAnrede",
		"FirmaName",
		"Str",
		"PLZ",
		"Ort",
		"Stelle",
		"Rueckmeldung",
	}
	ttargs1 := []string{
		"1",
		"15",
		"15.04.2021",
		"Franco-Midland Hardware Co.",
		"Herr",
		"Pinner",
		"Hauptstr. 42",
		"12345",
		"Neustadt",
		"Geschäftsführer",
		"Einladung zum Vorstellungsgespräch",
	}
	ttargs.anschriften = append(ttargs.anschriften, ttargs0)
	ttargs.anschriften = append(ttargs.anschriften, ttargs1)
	var ttwant []string
	ttwant = []string{
		"%%",
		"%% \\CharacterTable",
		"%%  {Upper-case    \\A\\B\\C\\D\\E\\F\\G\\H\\I\\J\\K\\L\\M\\N\\O\\P\\Q\\R\\S\\T\\U\\V\\W\\X\\Y\\Z",
		"%%   Lower-case    \\a\\b\\c\\d\\e\\f\\g\\h\\i\\j\\k\\l\\m\\n\\o\\p\\q\\r\\s\\t\\u\\v\\w\\x\\y\\z",
		"%%   Digits        \\0\\1\\2\\3\\4\\5\\6\\7\\8\\9",
		"%%   Exclamation   \\!     Double quote  \\\"     Hash (number) \\#",
		"%%   Dollar        \\$     Percent       \\%     Ampersand     \\&",
		"%%   Acute accent  \\'     Left paren    \\(     Right paren   \\)",
		"%%   Asterisk      \\*     Plus          \\+     Comma         \\,",
		"%%   Minus         \\-     Point         \\.     Solidus       \\/",
		"%%   Colon         \\:     Semicolon     \\;     Less than     \\<",
		"%%   Equals        \\=     Greater than  \\>     Question mark \\?",
		"%%   Commercial at \\@     Left bracket  \\[     Backslash     \\\\",
		"%%   Right bracket \\]     Circumflex    \\^     Underscore    \\_",
		"%%   Grave accent  \\`     Left brace    \\{     Vertical bar  \\|",
		"%%   Right brace   \\}     Tilde         \\~}",
		"%% zusätzliche",
		"%% Farbdefinitionen:",
		"%% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +",
		"%%                                                                                                 +",
		"%% Konfiguration der eigenen Daten                                                                 +",
		"%%                                                                                                 +",
		"\\Name{Mustermann}",
		"\\Vorname{Max}",
		"\\Street{Musterstraße 1}",
		"\\Plz{12345}",
		"\\Stadt{Musterdorf}",
		"\\MeinBeruf{DevSecOps Engineer}",
		"\\EMail{max@mustermann.com}",
		"%%\\Tel{01234~567890}",
		"\\Mobile{0123~4567890}",
		"\\Sta{deutsch}",
		"\\GebDatum{01.01.2000}",
		"%%                                                                                                 +",
		"%% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +",
		"%%                                                                                                 +",
		"%% +----Achtung--------------------------------------+",
		"%% + ID meint (Zeilennummer-1) und nicht das Feld id +",
		"%% + Bsp: Eintrag in Zeile 48: ID 47                 +",
		"%% +                                                 +",
		"%% + Außer der Klasse ahilbig-bewerbung wird die     +",
		"%% + Option idPlain mitgegeben. Dann wird nach dem   +",
		"%% + exakten Match im Feld id gesucht.               +",
		"\\ID{1}",
		"%% +-------------------------------------------------+",
		"\\Anhang{Ausbildungszeugnis.\\newline Arbeitszeugnis.\\newline Zertifikate.\\newline }{%",
		"\\item Ausbildungszeugnis",
		"\\item Arbeitszeugnis",
		"\\item Zertifikate",
		"}",
		"\\endinput",
		"%%",
		"%% End of file `config.inc'.",
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{"Test correct config creation", ttargs, ttwant},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := createConfigContent(tt.args.bcfg, tt.args.anschriften); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("createConfigContent() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_createCsvEntry(t *testing.T) {
	type args struct {
		csvPath      string
		date         string
		firma        string
		anrede       string
		kontakt      string
		strasse      string
		plz          string
		ort          string
		position     string
		rueckmeldung string
	}
	var ttargs args
	ttargs.csvPath = ".testfiles/anschrift.csv"
	ttargs.date = *dateflag
	ttargs.firma = *firmaflag
	ttargs.anrede = *anredeflag
	ttargs.kontakt = *nameflag
	ttargs.strasse = *strasseflag
	ttargs.plz = *plzflag
	ttargs.ort = *ortflag
	ttargs.position = *positionsflag
	ttargs.rueckmeldung = *rueckmeldungsflag
	var ttwant [][]string
	ttwant0 := []string{
		"ID",
		"KW",
		"Datum",
		"Firma",
		"FirmaAnrede",
		"FirmaName",
		"Str",
		"PLZ",
		"Ort",
		"Stelle",
		"Rueckmeldung",
	}
	ttwant1 := []string{
		"1",
		"15",
		"15.04.2021",
		"Franco-Midland Hardware Co.",
		"Herr",
		"Pinner",
		"Hauptstr. 42",
		"12345",
		"Neustadt",
		"Geschäftsführer",
		"Einladung zum Vorstellungsgespräch",
	}
	var currentdate = time.Now()
	ts, _ := time.Parse("02.01.2006", currentdate.Format("02.01.2006"))
	_, week := ts.ISOWeek()
	ttwant2 := []string{
		"2",
		strconv.Itoa(week),
		currentdate.Format("02.01.2006"),
		"Musterfirma",
		"Sehr geehrte Damen und Herren",
		"",
		"Musterstraße 1",
		"12345",
		"Musterhausen",
		"DevSecOps Engineer",
		"beworben",
		"",
	}
	ttwant = append(ttwant, ttwant0)
	ttwant = append(ttwant, ttwant1)
	ttwant = append(ttwant, ttwant2)
	tests := []struct {
		name    string
		args    args
		want    [][]string
		wantErr bool
	}{
		{"Create Array anschriften for anschrift.csv", ttargs, ttwant, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := createCsvEntry(tt.args.csvPath, tt.args.date, tt.args.firma, tt.args.anrede, tt.args.kontakt, tt.args.strasse, tt.args.plz, tt.args.ort, tt.args.position, tt.args.rueckmeldung)
			if (err != nil) != tt.wantErr {
				t.Errorf("createCsvEntry() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("createCsvEntry() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_createApplicationLayoutTidyFirma(t *testing.T) {
	type args struct {
		firma string
	}
	var ttargs args
	ttargs.firma = "Musterfirma GmbH & Co/KG"
	tests := []struct {
		name string
		args args
		want string
	}{
		{"Check removing unwanted Symbols", ttargs, "Musterfirma_GmbH_CoKG"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := createApplicationLayoutTidyFirma(tt.args.firma); got != tt.want {
				t.Errorf("createApplicationLayoutTidyFirma() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_createApplicationLayout(t *testing.T) {
	type args struct {
		firma     string
		language  string
		configinc []string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := createApplicationLayout(tt.args.firma, tt.args.language, tt.args.configinc); (err != nil) != tt.wantErr {
				t.Errorf("createApplicationLayout() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_createLangContent(t *testing.T) {
	type args struct {
		lang   string
		firma  string
		folder string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := createLangContent(tt.args.lang, tt.args.firma, tt.args.folder); (err != nil) != tt.wantErr {
				t.Errorf("createLangContent() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_findtexfiles(t *testing.T) {
	type args struct {
		folder string
	}
	var ttargs args
	ttargs.folder = "./.testfiles"
	var ttargs2 args
	ttargs2.folder = "./.tests"
	var ttwant []string
	ttwant = append(ttwant, "bewerbung.tex")
	ttwant = append(ttwant, "bewerbung_en.tex")
	ttwant = append(ttwant, "eurocv.tex")
	var ttwant2 []string
	tests := []struct {
		name    string
		args    args
		want    []string
		wantErr bool
	}{
		{"Testing TeX-file-finder", ttargs, ttwant, false},
		{"Testing non-existing-folder", ttargs2, ttwant2, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := findtexfiles(tt.args.folder)
			if (err != nil) != tt.wantErr {
				t.Errorf("findtexfiles() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("findtexfiles() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_compiletexfiles(t *testing.T) {
	type args struct {
		firma string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := compiletexfiles(tt.args.firma); (err != nil) != tt.wantErr {
				t.Errorf("compiletexfiles() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

// Examples
func Example_csvReader() {
	content, _ := csvReader(".testfiles/anschrift.csv")
	fmt.Println(content)
	// Output: [[ID KW Datum Firma FirmaAnrede FirmaName Str PLZ Ort Stelle Rueckmeldung] [1 15 15.04.2021 Franco-Midland Hardware Co. Herr Pinner Hauptstr. 42 12345 Neustadt Geschäftsführer Einladung zum Vorstellungsgespräch]]
}

func ExampleNewConfig() {
	config, _ := NewConfig(".testfiles/config.yaml")
	fmt.Println(config)
	// Output: &{{Mustermann Max Musterstraße 1 12345 Musterdorf DevSecOps Engineer max@mustermann.com 01234~567890 0123~4567890 deutsch 01.01.2000} [{Ausbildungszeugnis} {Arbeitszeugnis} {Zertifikate}]}
}

func Example_createConfigContent() {
	bewerbercfg, _ := NewConfig(".testfiles/config.yaml")
	anschriften, _ := csvReader(".testfiles/anschrift.csv")
	configContent := createConfigContent(bewerbercfg, anschriften)
	fmt.Println(configContent)
	// Output: [%% %% \CharacterTable %%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z %%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z %%   Digits        \0\1\2\3\4\5\6\7\8\9 %%   Exclamation   \!     Double quote  \"     Hash (number) \# %%   Dollar        \$     Percent       \%     Ampersand     \& %%   Acute accent  \'     Left paren    \(     Right paren   \) %%   Asterisk      \*     Plus          \+     Comma         \, %%   Minus         \-     Point         \.     Solidus       \/ %%   Colon         \:     Semicolon     \;     Less than     \< %%   Equals        \=     Greater than  \>     Question mark \? %%   Commercial at \@     Left bracket  \[     Backslash     \\ %%   Right bracket \]     Circumflex    \^     Underscore    \_ %%   Grave accent  \`     Left brace    \{     Vertical bar  \| %%   Right brace   \}     Tilde         \~} %% zusätzliche %% Farbdefinitionen: %% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - + %%                                                                                                 + %% Konfiguration der eigenen Daten                                                                 + %%                                                                                                 + \Name{Mustermann} \Vorname{Max} \Street{Musterstraße 1} \Plz{12345} \Stadt{Musterdorf} \MeinBeruf{DevSecOps Engineer} \EMail{max@mustermann.com} %%\Tel{01234~567890} \Mobile{0123~4567890} \Sta{deutsch} \GebDatum{01.01.2000} %%                                                                                                 + %% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - + %%                                                                                                 + %% +----Achtung--------------------------------------+ %% + ID meint (Zeilennummer-1) und nicht das Feld id + %% + Bsp: Eintrag in Zeile 48: ID 47                 + %% +                                                 + %% + Außer der Klasse ahilbig-bewerbung wird die     + %% + Option idPlain mitgegeben. Dann wird nach dem   + %% + exakten Match im Feld id gesucht.               + \ID{1} %% +-------------------------------------------------+ \Anhang{Ausbildungszeugnis.\newline Arbeitszeugnis.\newline Zertifikate.\newline }{% \item Ausbildungszeugnis \item Arbeitszeugnis \item Zertifikate } \endinput %% %% End of file `config.inc'.]
}

func Example_createCsvEntry() {
	csventry, _ := createCsvEntry(".testfiles/anschrift.csv", "28.07.2021", "Musterfirma GmbH & Co/KG", "Herr", "Musterag", "Musterstraße 1", "12345", "Musterstadt", "DevSecOps Engineer", "beworben")
	fmt.Println(csventry)
	// Output: [[ID KW Datum Firma FirmaAnrede FirmaName Str PLZ Ort Stelle Rueckmeldung] [1 15 15.04.2021 Franco-Midland Hardware Co. Herr Pinner Hauptstr. 42 12345 Neustadt Geschäftsführer Einladung zum Vorstellungsgespräch] [2 30 28.07.2021 Musterfirma GmbH & Co/KG Herr Musterag Musterstraße 1 12345 Musterstadt DevSecOps Engineer beworben ]]
}

func Example_createApplicationLayoutTidyFirma() {
	firma := createApplicationLayoutTidyFirma("Musterfirma GmbH & Co/KG")
	fmt.Println(firma)
	// Output: Musterfirma_GmbH_CoKG
}

func Example_findtexfiles() {
	texfiles, _ := findtexfiles(".testfiles")
	fmt.Println(texfiles)
	// Output: [bewerbung.tex bewerbung_en.tex eurocv.tex]
}
